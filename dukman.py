################################################################################################
# dukman: DUKPT manager. This tool can generate DUKPT initial keys and perform various other
# operations realated to DUKPT mechanism
# Author : Mohamed Ismail Bari
# Python version : 3.6.0
# Date: 21/Mar/2020
# Dependency: Cryptodome, string, click
###############################################################################################
# Version history
# ```````````````
# 0.0 - Initial version with IPEK and TR31 generation
# 0.1 - HMAC generation added
# 0.2 - Program name changed to dukman
# 0.3 - Added click.secho instead of print. click.secho can print charactes in ASCII colors
# 0.4 - Added dukpt decryption given the KSN, transaction number, IPEK4
# 0.5 - Corrected IPEK generation messages, BDK was mentioned 32 bytes but its 32 hex chars
#       KSN was mentioned 20 bytes but is 20 hex chars
###############################################################################################

from Cryptodome.Cipher import DES, DES3, AES
from Cryptodome.Hash import CMAC, HMAC, SHA256
from Cryptodome.Random import get_random_bytes
import string
import click
from bitstring import BitArray

# Global constants
DUKMAN_MAJOR_VERSION = 0
DUKMAN_MINOR_VERSION = 5

####################################################################################################
# TR31 base class
####################################################################################################
class TR31:
    BINDING_METHOD_TDEA_2KEY = 0
    BINDING_METHOD_TDEA_3KEY = 1
    BINDING_METHOD_AES_128 = 2
    BINDING_METHOD_AES_192 = 3
    BINDING_METHOD_AES_256 = 4

    CMAC_TDES_SIZE_IN_BYTES = 8

    Key_Binding = {"A": "A",
                   "a": "A",
                   "B": "B",
                   "b": "B",
                   "C": "C",
                   "c": "C",
                   "D": "D",
                   "d": "D"}
    
    Key_Usage = {"1": "B0",
                 "2": "B1",
                 "3": "B2",
                 "4": "C0",
                 "5": "D0",
                 "6": "D1",
                 "7": "D2",
                 "8": "E0",
                 "9": "E1",
                 "a": "E2",
                 "b": "E3",
                 "c": "E4",
                 "d": "E5",
                 "e": "E6",
                 "f": "I0",
                 "g": "K0",
                 "h": "K1",
                 "i": "K2",
                 "j": "K3",
                 "k": "M0",
                 "l": "M1",
                 "m": "M2",
                 "n": "M3",
                 "o": "M4",
                 "p": "M5",
                 "q": "M6",
                 "r": "M7",
                 "s": "M8",
                 "t": "P0",
                 "u": "S0",
                 "v": "S1",
                 "w": "S2",
                 "x": "V0",
                 "y": "V1",
                 "z": "V2",
                 "A": "V3",
                 "B": "V4"}
                 
    Algorithm = {"A": "A",
                 "D": "D",
                 "E": "E",
                 "H": "H",
                 "R": "R",
                 "S": "S",
                 "T": "T",
                 "a": "A",
                 "d": "D",
                 "e": "E",
                 "h": "H",
                 "r": "R",
                 "s": "S",
                 "t": "T"}

    Mode_of_Use = {"B": "B",
                   "C": "C",
                   "D": "D",
                   "E": "E",
                   "G": "G",
                   "N": "N",
                   "S": "S",
                   "T": "T",
                   "V": "V",
                   "X": "X",
                   "Y": "Y",
                   "b": "B",
                   "c": "C",
                   "d": "D",
                   "e": "E",
                   "g": "G",
                   "n": "N",
                   "s": "S",
                   "t": "T",
                   "v": "V",
                   "x": "X",
                   "y": "Y"}

    Exportability = {"E": "E",
                     "N": "N",
                     "S": "S",
                     "e": "E",
                     "n": "N",
                     "s": "S"}
    
    ################################################################################################
    # Constructor
    ################################################################################################
    def __init__(self):
        return

    ################################################################################################
    # Returns the Encryption key
    ################################################################################################
    def Derive_Encryption_Key(self, _KBPK, _Binding_Method):
        _retval = False
        _Encryption_Key = b'\x00'
        if _Binding_Method == self.BINDING_METHOD_TDEA_2KEY:
            _Key_Derivation_Data = b'\x01\x00\x00\x00\x00\x00\x00\x80'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=DES3)
            _Encryption_Key_1st_8bytes = _CMAC.digest()
            _Key_Derivation_Data = b'\x02\x00\x00\x00\x00\x00\x00\x80'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=DES3)
            _Encryption_Key_2nd_8bytes = _CMAC.digest()
            _Encryption_Key = _Encryption_Key_1st_8bytes + _Encryption_Key_2nd_8bytes
            _retval = True
        elif _Binding_Method == self.BINDING_METHOD_TDEA_3KEY:
            _Key_Derivation_Data = b'\x01\x00\x00\x00\x00\x01\x00\xC0'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=DES3)
            _Encryption_Key_1st_8bytes = _CMAC.digest()
            _Key_Derivation_Data = b'\x02\x00\x00\x00\x00\x01\x00\xC0'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=DES3)
            _Encryption_Key_2nd_8bytes = _CMAC.digest()
            _Key_Derivation_Data = b'\x03\x00\x00\x00\x00\x01\x00\xC0'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=DES3)
            _Encryption_Key_3rd_8bytes = _CMAC.digest()
            _Encryption_Key = _Encryption_Key_1st_8bytes + _Encryption_Key_2nd_8bytes + _Encryption_Key_3rd_8bytes
            _retval = True
        elif _Binding_Method == self.BINDING_METHOD_AES_128:
            _Key_Derivation_Data = b'\x01\x00\x00\x00\x00\x02\x00\x80'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=AES)
            _Encryption_Key = _CMAC.digest()
            _retval = True
        elif _Binding_Method == self.BINDING_METHOD_AES_192:
            _Key_Derivation_Data = b'\x01\x00\x00\x00\x00\x03\x00\xC0'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=AES)
            _Encryption_Key_1st_16bytes = _CMAC.digest()
            _Key_Derivation_Data = b'\x02\x00\x00\x00\x00\x03\x00\xC0'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=AES)
            _Encryption_Key_2nd_16bytes = _CMAC.digest()
            _Encryption_Key = _Encryption_Key_1st_16bytes + _Encryption_Key_2nd_16bytes[:8]
            _retval = True
        elif _Binding_Method == self.BINDING_METHOD_AES_256:
            _Key_Derivation_Data = b'\x01\x00\x00\x00\x00\x03\x01\x00'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=AES)
            _Encryption_Key_1st_16bytes = _CMAC.digest()
            _Key_Derivation_Data = b'\x02\x00\x00\x00\x00\x03\x01\x00'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=AES)
            _Encryption_Key_2nd_16bytes = _CMAC.digest()
            _Encryption_Key = _Encryption_Key_1st_16bytes + _Encryption_Key_2nd_16bytes
            _retval = True
        else:
            _retval = False

        return _retval, _Encryption_Key

    ################################################################################################
    # Returns the MAC key
    # Note that the CMAC module internally calculates K1/K2 subkeys. However, for CMAC calculation
    # we need to have access to the K1/K2 subkeys.
    # The sub keys can be generated using Derive_MAC_Subkeys()
    ################################################################################################
    def Derive_MAC_Key(self, _KBPK, _Binding_Method):
        _retval = False
        _MAC_Key = b'\x00'
        if _Binding_Method == self.BINDING_METHOD_TDEA_2KEY:
            _Key_Derivation_Data = b'\x01\x00\x01\x00\x00\x00\x00\x80'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=DES3)
            _MAC_Key_1st_8bytes = _CMAC.digest()
            _Key_Derivation_Data = b'\x02\x00\x01\x00\x00\x00\x00\x80'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=DES3)
            _MAC_Key_2nd_8bytes = _CMAC.digest()
            _MAC_Key = _MAC_Key_1st_8bytes + _MAC_Key_2nd_8bytes
            _retval = True
        elif _Binding_Method == self.BINDING_METHOD_TDEA_3KEY:
            _Key_Derivation_Data = b'\x01\x00\x01\x00\x00\x01\x00\xC0'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=DES3)
            _MAC_Key_1st_8bytes = _CMAC.digest()
            _Key_Derivation_Data = b'\x02\x00\x01\x00\x00\x01\x00\xC0'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=DES3)
            _MAC_Key_2nd_8bytes = _CMAC.digest()
            _Key_Derivation_Data = b'\x03\x00\x01\x00\x00\x01\x00\xC0'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=DES3)
            _MAC_Key_3rd_8bytes = _CMAC.digest()
            _MAC_Key = _MAC_Key_1st_8bytes + _MAC_Key_2nd_8bytes + _MAC_Key_3rd_8bytes
            _retval = True
        elif _Binding_Method == self.BINDING_METHOD_AES_128:
            _Key_Derivation_Data = b'\x01\x00\x01\x00\x00\x02\x00\x80'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=AES)
            _MAC_Key = _CMAC.digest()
            _retval = True
        elif _Binding_Method == self.BINDING_METHOD_AES_192:
            _Key_Derivation_Data = b'\x01\x00\x01\x00\x00\x03\x00\xC0'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=AES)
            _MAC_Key_1st_16bytes = _CMAC.digest()
            _Key_Derivation_Data = b'\x02\x00\x01\x00\x00\x03\x00\xC0'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=AES)
            _MAC_Key_2nd_16bytes = _CMAC.digest()
            _MAC_Key = _MAC_Key_1st_16bytes + _MAC_Key_2nd_16bytes[:8]
            _retval = True
        elif _Binding_Method == self.BINDING_METHOD_AES_256:
            _Key_Derivation_Data = b'\x01\x00\x01\x00\x00\x03\x01\x00'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=AES)
            _MAC_Key_1st_16bytes = _CMAC.digest()
            _Key_Derivation_Data = b'\x02\x00\x01\x00\x00\x03\x01\x00'
            _CMAC = CMAC.new(key=_KBPK, msg=_Key_Derivation_Data, ciphermod=AES)
            _MAC_Key_2nd_16bytes = _CMAC.digest()
            _MAC_Key = _MAC_Key_1st_16bytes + _MAC_Key_2nd_16bytes
            _retval = True
        else:
            _retval = False

        return _retval, _MAC_Key

    ################################################################################################
    # Returns the MAC subkeys
    # Note that the CMAC module internally calculates K1/K2 subkeys. However, for CMAC calculation
    # we need to have access to the K1/K2 subkeys.
    # The sub keys can be generated using Derive_MAC_Subkeys()
    # Please refer the following docs on how MAC Subkeys are generated
    # ASC+X9+TR+31-2018, NIST.SP.800-38b
    ################################################################################################
    @staticmethod
    def Derive_CMAC_Subkeys(_CMAC_Key):
        _Data = b'\x00\x00\x00\x00\x00\x00\x00\x00'
        _DES3 = DES3.new(key=_CMAC_Key, mode=DES3.MODE_ECB)
        _Cipher_Byte = _DES3.encrypt(_Data)
        _Cipher = list(_Cipher_Byte)

        _First_Byte = _Cipher[0]
        for _Index in range(DES3.block_size):
            _Cipher[_Index] <<= 1
            if _Index < (DES3.block_size - 1) and (_Cipher[_Index + 1] & 0x80):
                _Cipher[_Index] |= 0x01
            _Cipher[_Index] &= 0xFF

        if _First_Byte & 0x80:
            _Cipher[DES3.block_size - 1] ^= 0x1B
        _K1 = bytes(_Cipher)

        _First_Byte = _Cipher[0]
        for _Index in range(DES3.block_size):
            _Cipher[_Index] <<= 1
            if _Index < (DES3.block_size - 1) and (_Cipher[_Index + 1] & 0x80):
                _Cipher[_Index] |= 0x01
            _Cipher[_Index] &= 0xFF

        if _First_Byte & 0x80:
            _Cipher[DES3.block_size - 1] ^= 0x1B
        _K2 = bytes(_Cipher)

        return _K1, _K2

    ################################################################################################
    # XORs two iterable variables
    ################################################################################################
    def XOR_Bytes(self, _iterable_var1, _iterable_var2):
        # zip() creates tuples of two iterable variables.
        # Note that the tuples are constructed up to the iterable variable
        # with the lowest number of elements
        return bytes([_element_on_var1 ^ _element_on_var2 for _element_on_var1, _element_on_var2 in
                      zip(_iterable_var1, _iterable_var2)])

    ################################################################################################
    # Calculates CMAC, given the keys and data
    # Note that the CMAC generation is different from the standard CMAC available in Cryptodome
    # package. K1 or K2 will be XORed for the last block depending on the padding
    # All parameters shall be byte type
    ################################################################################################
    def Calculate_CMAC(self, _MAC_Key, _K1, _K2, _Data):
        # ------------------------------------------------------------------------------------------
        # For DES algorithm
        # ------------------------------------------------------------------------------------------
        _isPadding_Done = False
        # It would have been better to convert _Data into string and deal with characters rather than bytes.
        # This way it would avoid confusion. However, I did this just to show we can do with bytes as well
        #print("-" * 20)
        #print("CMAC Calc")
        # Check if the _Data size is a multiple of blocksize. If not pad it
        while True:
            if (len(_Data) * 2) % DES3.block_size:  # * 2 because _Data is byte type. Each byte is 2 characters
                if not _isPadding_Done:
                    _Data += b'\x80'
                    _isPadding_Done = True
                else:
                    _Data += b'\x00'
            else:
                break

        #print(_MAC_Key.hex().upper())
        #print(_Data.hex().upper())
        #print(len(_Data))

        # Perform the CMAC calculation
        # Refer doc ASC X9 TR 31-2018, pg 42
        _DES3 = DES3.new(key=_MAC_Key, mode=DES3.MODE_ECB)
        _CMAC = b'\x00\x00\x00\x00\x00\x00\x00\x00'
        for _Index in range(0, (len(_Data) - DES3.block_size), DES3.block_size):
            #print(_Data[_Index: _Index + DES3.block_size].hex().upper() + " => ", end="")
            _Data_to_Encrypt = self.XOR_Bytes(_CMAC, _Data[_Index: _Index + DES3.block_size])
            _CMAC = _DES3.encrypt(_Data_to_Encrypt)
            #print(_Data_to_Encrypt.hex().upper() + " => ", end="")
            #print(_CMAC.hex().upper())

        # Refer doc ASC X9 TR 31-2018, pg 42
        # Note that K1 is used for last block XOR if no padding is done
        # K2 is used if padding is done.
        _Last_Block = _Data[len(_Data) - DES3.block_size:]
        if _isPadding_Done:
            # The last block is XORed with K2 if padding is done
            _Last_Block = self.XOR_Bytes(_Last_Block, _K2)
        else:
            # The last block is XORed with K1 if no padding is done
            _Last_Block = self.XOR_Bytes(_Last_Block, _K1)

        # Then the data is XORed with the CMAC calculated so far
        _Data_to_Encrypt = self.XOR_Bytes(_CMAC, _Last_Block)
        # Then the data is encrypted to obtain the final CMAC
        _CMAC = _DES3.encrypt(_Data_to_Encrypt)
        #print(_CMAC.hex().upper())
        #print("-" * 20)
        return _CMAC

####################################################################################################
# TR31 generator class
####################################################################################################
class TR31_Generator(TR31):
    ################################################################################################
    # Constructor
    ################################################################################################
    def __init__(self):
        return

    def Get_Key_Binding(self):
        print("#" * 80)
        print("# Key Binding")
        print("#" * 80)
        print("[A] => Key Variant Binding")
        print("[B] => TDEA Key Derivation Binding")
        print("[C] => TDEA Key Variant Binding")
        print("[D] => AES Key Derivation Binding")
        _option = input("Key Usage choice : ")
        _retval = self.Key_Binding.get(_option)
        return _retval

    ################################################################################################
    # Returns the Key_Usage indicator. Returns "None" if an unavailable option is selected
    ################################################################################################
    def Get_Key_Usage(self):
        print("#" * 80)
        print("# Key Usage")
        print("#" * 80)
        print("[1] B0 => BDK Base Derivation Key")
        print("[2] B1 => Initial DUKPT Key")
        print("[3] B2 => Base Key Variant Key")
        print("[4] C0 => CVK Card Verification Key")
        print("[5] D0 => Symmetric Key for Data Encryption")
        print("[6] D1 => Asymmetric Key for Data Encryption")
        print("[7] D2 => Data Encryption Key for Decimalization Table")
        print("[8] E0 => EMV/chip Issuer Master Key: Application cryptograms")
        print("[9] E1 => EMV/chip Issuer Master Key: Secure Messaging for Confidentiality")
        print("[a] E2 => EMV/chip Issuer Master Key: Secure Messaging for Integrity")
        print("[b] E3 => EMV/chip Issuer Master Key: Data Authentication Code")
        print("[c] E4 => EMV/chip Issuer Master Key: Dynamic Numbers")
        print("[d] E5 => EMV/chip Issuer Master Key: Card Personalization")
        print("[e] E6 => EMV/chip Issuer Master Key: Other")
        print("[f] I0 => Initialization Vector (IV)")
        print("[g] K0 => Key Encryption or wrapping")
        print("[h] K1 => TR-31 Key Block Protection Key")
        print("[i] K2 => TR-34 Asymmetric key")
        print("[j] K3 => Asymmetric key for key agreement/key wrapping")
        print("[k] M0 => ISO 16609 MAC algorithm 1 (using TDEA)")
        print("[l] M1 => ISO 9797-1 MAC Algorithm 1")
        print("[m] M2 => ISO 9797-1 MAC Algorithm 2")
        print("[n] M3 => ISO 9797-1 MAC Algorithm 3")
        print("[o] M4 => ISO 9797-1 MAC Algorithm 4")
        print("[p] M5 => ISO 9797-1:1999 MAC Algorithm 5")
        print("[q] M6 => ISO 9797-1:2011 MAC Algorithm 5/CMAC")
        print("[r] M7 => HMAC")
        print("[s] M8 => ISO 9797-1:2011 MAC Algorithm 6")
        print("[t] P0 => PIN Encryption")
        print("[u] S0 => Asymmetric key pair for digital signature")
        print("[v] S1 => Asymmetric key pair, CA key")
        print("[w] S2 => Asymmetric key pair, nonX9.24 key")
        print("[x] V0 => PIN verification, KPV, other algorithm")
        print("[y] V1 => PIN verification, IBM 3624")
        print("[z] V2 => PIN Verification, VISA PVV")
        print("[A] V3 => PIN Verification, X9.132 algorithm 1")
        print("[B] V4 => PIN Verification, X9.132 algorithm 2")
        _option = input("Key Usage choice : ")
        _retval = self.Key_Usage.get(_option)
        return _retval

    ################################################################################################
    # Returns the Algorithm indicator. Returns "None" if an unavailable option is selected
    ################################################################################################
    def Get_Algorithm(self):
        print("#" * 80)
        print("# Algorithm")
        print("#" * 80)
        print("[A] => AES")
        print("[D] => DEA (Note that this is included for backwards compatibility)")
        print("[E] => Elliptic Curve")
        print("[H] => HMAC(specify the underlying hash algorithm in optional field)")
        print("[R] => RSA")
        print("[S] => DSA (Note that this is included for future reference)")
        print("[T] => Triple DEA (TDEA)")
        _option = input("Key Usage choice : ")
        _retval = self.Algorithm.get(_option)
        return _retval

    ################################################################################################
    # Returns the Mode_of_Use indicator. Returns "None" if an unavailable option is selected
    ################################################################################################
    def Get_Mode_Of_Use(self):
        print("#" * 80)
        print("# Mode of Use")
        print("#" * 80)
        print("[B] => Both Encrypt & Decrypt / Wrap & Unwrap")
        print("[C] => Both Generate & Verify")
        print("[D] => Decrypt / Unwrap Only")
        print("[E] => Encrypt / Wrap Only")
        print("[G] => Generate Only")
        print("[N] => No special restrictions (other than restrictions implied by the Key Usage)")
        print("[S] => Signature Only")
        print("[T] => Both Sign & Decrypt")
        print("[V] => Verify Only")
        print("[X] => Key used to derive other key(s)")
        print("[Y] => Key used to create key variants")
        _option = input("Key Usage choice : ")
        _retval = self.Mode_of_Use.get(_option)
        return _retval

    ################################################################################################
    # Returns the Key_version indicator. Returns "None" if an unavailable option is selected
    ################################################################################################
    @staticmethod
    def Get_Key_Version():
        print("#" * 80)
        print("# Key Version")
        print("#" * 80)
        _retval = input("Key version : ")

        if len(_retval) > 2:
            _retval = _retval[:2]
        else:
            _retval = _retval.zfill(2)

        return _retval

    ################################################################################################
    # Returns the Exportability indicator. Returns "None" if an unavailable option is selected
    ################################################################################################
    def Get_Exportability(self):
        print("#" * 80)
        print("# Exportability")
        print("#" * 80)
        print("[E] => Exportable")
        print("[N] => Non-exportable")
        print("[S] => Sensitive / Exportable")
        _option = input("Key Usage choice : ")
        _retval = self.Exportability.get(_option)
        return _retval

    ################################################################################################
    # Returns the user key to be wrapped into TR31 block
    ################################################################################################
    @staticmethod
    def Get_Key_to_Wrap():
        print("#" * 80)
        print("# User Key to be wrapped in TR31 keyblock")
        print("#" * 80)
        # We do not need to check for the length of the key to wrap. The key is just treated as
        # a data of arbitrary length
        while True:
            _retval = input("TR31 User key : ")
            if not all(_chars in string.hexdigits for _chars in _retval):
                click.secho("Error : Non-Hex character found!", fg="red")
            else:
                break
        return _retval

    ################################################################################################
    # Returns the KBPK
    ################################################################################################
    @staticmethod
    def Get_KBPK():
        print("#" * 80)
        print("# User KBPK")
        print("#" * 80)
        while True:
            _retval = input("KBPK (32 bytes):")
            if len(_retval) == 32:
                if not all(_chars in string.hexdigits for _chars in _retval):
                    click.secho("Error : Non-Hex character found!", fg="red")
                else:
                    if _retval[:16] == _retval[16:]:
                        click.secho("Error : Triple DES key degenerates to singed DES!", fg="red")
                    else:
                        break
            else:
                click.secho("Error: KBPK shall be 32 hex characters!", fg="red")
        return _retval

    ################################################################################################
    # Gets the user input for generating the TR31 header
    ################################################################################################
    def Get_All_User_Input(self):
        _Header = None
        _Key_to_be_Wrapped = None
        _KBPK = None

        while True:
            _value = self.Get_Key_Binding()
            if _value is None:
                click.secho("Invalid input!", fg="red")
                _Header = None
                break
            _Header = _value

            _Header = _Header + "LLLL"                               # Key Length to be filled in after the TR31 block is complete

            _value = self.Get_Key_Usage()
            if _value is None:
                click.secho("Invalid input!", fg="red")
                _Header = None
                break
            _Header = _Header + _value

            _value = self.Get_Algorithm()
            if _value is None:
                prclick.secho("Invalid input!", fg="red")
                _Header = None
                break
            _Header = _Header + _value

            _value = self.Get_Mode_Of_Use()
            if _value is None:
                click.secho("Invalid input!", fg="red")
                _Header = None
                break
            _Header = _Header + _value

            _value = self.Get_Key_Version()
            if _value is None:
                click.secho("Invalid input!", fg="red")
                _Header = None
                break
            _Header = _Header + _value

            _value = self.Get_Exportability()
            if _value is None:
                click.secho("Invalid input!", fg="red")
                _Header = None
                break
            _Header = _Header + _value

            _Header = _Header + "0000"                               # Number of optional blocks set to 0000

            _value = self.Get_Key_to_Wrap()
            if _value is None:
                click.secho("Invalid input!", fg="red")
                break
            _Key_to_be_Wrapped = _value

            _value = self.Get_KBPK()
            if _value is None:
                click.secho("Invalid input!", fg="red")
                break
            _KBPK = _value

            break

        return _Header, _Key_to_be_Wrapped, _KBPK

    ################################################################################################
    # Performs the generation of TR31 key block
    ################################################################################################
    def Generate(self):
        while True:
            _TR31_Block = None
            # Get all the required inputs from the user
            _Header_Str, _Key_to_be_Wrapped_Str, _KBPK_Str = self.Get_All_User_Input()
            if _Header_Str is None or _Key_to_be_Wrapped_Str is None or _KBPK_Str is None:
                break

            # Check which key block binding method is used
            if _Header_Str[0] == 'D':
                _Key_Size = 32
                _Binding_Method = self.BINDING_METHOD_AES_128
            else:
                _Key_Size = 16
                _Binding_Method = self.BINDING_METHOD_TDEA_2KEY

            if (len(_Key_to_be_Wrapped_Str) < _Key_Size) or (len(_KBPK_Str) < _Key_Size):
                click.secho("Error: Either the TR31_User_key or the KPBK_Key length is small", fg="red")
                break

            # Generate the Encryption_Key
            _KBPK = bytes.fromhex(_KBPK_Str)
            _status, _Encryption_Key = self.Derive_Encryption_Key(_KBPK, _Binding_Method)
            if not _status:
                break

            # Generate the MAC_Key
            _status, _MAC_Key = self.Derive_MAC_Key(_KBPK, _Binding_Method)
            if not _status:
                break

            # CMAC subkeys K1 and K2 is used for deriving the _Encryption_Key and the MAC_Key.
            # However, it is not used here as the Cryptodome CMAC function is internally deriving it
            # K1 and K2 are just calculated for reporting purpose only
            _K1, _K2 = self.Derive_CMAC_Subkeys(_KBPK)
            _KM1, _KM2 = self.Derive_CMAC_Subkeys(_MAC_Key)

            print("#" * 80)
            print("# Generating TR31 block")
            print("#" * 80)
            click.secho("KBPK : " + _KBPK.hex().upper(), fg="green")
            click.secho("KBPK CMAC Subkey K1 : " + _K1.hex().upper(), fg="green")
            click.secho("KBPK CMAC Subkey K2 : " + _K2.hex().upper(), fg="green")
            click.secho("Encryption key : " + _Encryption_Key.hex().upper(), fg="green")
            click.secho("MAC key : " + _MAC_Key.hex().upper(), fg="green")
            click.secho("MAC Subkey KM1 = " + _KM1.hex().upper(), fg="green")
            click.secho("MAC Subkey KM2 = " + _KM2.hex().upper(), fg="green")

            # Prepare Key data to be encrypted along with padding
            # +------------+----------+----------+
            # | Key Length | Key data ! Padding  |
            # +------------+----------+----------+
            _Key_to_be_Wrapped_Len_Str = "%04X" % (len(_Key_to_be_Wrapped_Str) * 4)   # 4 bits per hex character
            _Raw_Block_no_Header_Str = _Key_to_be_Wrapped_Len_Str + _Key_to_be_Wrapped_Str   # + "1C2965473CE2"

            _Padding_Size = int(DES3.block_size - (len(_Raw_Block_no_Header_Str) / 2) % DES3.block_size)  # /2 as 2 chars per byte
            if _Padding_Size:
                _Rand_Bytes = get_random_bytes(_Padding_Size)
                _Raw_Block_no_Header_Str += _Rand_Bytes.hex().upper()

            # Prepare the clear TR31 block with header
            # +--------+------------+----------+----------+
            # | Header | Key Length | Key data ! Padding  |
            # +--------+------------+----------+----------+
            _Raw_Block_with_Header_Str = _Header_Str + _Raw_Block_no_Header_Str
            _Raw_Block_Len_Str = "%04d" % (len(_Raw_Block_with_Header_Str) + (self.CMAC_TDES_SIZE_IN_BYTES * 2))  # *2 because 2 chars per byte
            _Header_Str = _Header_Str[0] + _Raw_Block_Len_Str + _Header_Str[5:]

            # The _Raw_Block_with_Header for CMAC should have each character represented in ASCII.
            _Raw_Block_with_Header = _Header_Str.encode(encoding="ascii") + bytes.fromhex(_Raw_Block_no_Header_Str)
            click.secho("TR31 clear header + data for CMAC calc : " + _Raw_Block_with_Header.hex().upper(), fg="green")

            # Calculate the CMAC of the clear TR31 block with header - Using MAC_Key
            _TR31_Block_MAC = self.Calculate_CMAC(_MAC_Key, _KM1, _KM2, _Raw_Block_with_Header)

            #_CMAC = CMAC.new(key=_MAC_Key, msg=_Raw_Block_with_Header, ciphermod=DES3)
            #_TR31_Block_MAC = _CMAC.digest()

            # Encrypt the key_length + key_data with CMAC as the IV - Using Encryption_Key
            # Note that the data is encrypted in binary form
            _Raw_Block_no_Header = bytes.fromhex(_Raw_Block_no_Header_Str)
            _DES3 = DES3.new(key=_Encryption_Key, iv=_TR31_Block_MAC, mode=DES3.MODE_CBC)
            _TR31_Cipher = _DES3.encrypt(_Raw_Block_no_Header)

            click.secho("TR31 clear data : " + _Raw_Block_no_Header_Str.upper(), fg="green")
            click.secho("Header : " + _Header_Str.upper(), fg="green")
            click.secho("TR31 Cipher data : " + _TR31_Cipher.hex().upper(), fg="green")
            click.secho("TR31 MAC : " + _TR31_Block_MAC.hex().upper(), fg="green")

            _TR31_Cipher_Str = _TR31_Cipher.hex().upper()
            _TR31_Block_MAC_Str = _TR31_Block_MAC.hex().upper()
            _TR31_Block_Str = _Header_Str + _TR31_Cipher_Str + _TR31_Block_MAC_Str
            #_TR31_Block_Len_Str = "%04d" % len(_TR31_Block_Str)
            #_TR31_Block_Str = _TR31_Block_Str[0] + _TR31_Block_Len_Str + _TR31_Block_Str[5:]
            click.secho("TR31 Block : " + _TR31_Block_Str, fg="bright_yellow")
            break
            return _TR31_Block_Str

####################################################################################################
# IPEK Generator class
####################################################################################################
class IPEK_Generator:
    ################################################################################################
    # Returns BDK from user input
    ################################################################################################
    @staticmethod
    def Get_BDK():
        while True:
            _retval = input("BDK (32 hex characters, 16 bytes):")
            if len(_retval) == 32:
                if not all(_chars in string.hexdigits for _chars in _retval):
                    click.secho("Error : Non-Hex character found!", fg="red")
                else:
                    if _retval[:16] == _retval[16:]:
                        click.secho("Error : Triple DES key degenerates to singed DES!", fg="red")
                    else:
                        break
            else:
                click.secho("Error: BDK shall be 32 hex characters!", fg="red")
        return _retval

    ################################################################################################
    # Returns KSN from user input
    ################################################################################################
    @staticmethod
    def Get_KSN():
        while True:
            _retval = input("KSN (20 hex characters, 10 bytes):")
            if len(_retval) == 20:
                if not all(_chars in string.hexdigits for _chars in _retval):
                    click.secho("Error : Non-Hex character found!", fg="red")
                else:
                    break
            else:
                click.secho("Error: KSN shall be 20 hex characters!", fg="red")
        return _retval

    ################################################################################################
    # Returns the IKSN. The last 21 bits of the transaction counter is set to 0
    # _KSN is byte type
    ################################################################################################
    @staticmethod
    def Get_IKSN(_KSN):
        _KSN_List = list(_KSN)
        _IKSN_List = _KSN_List[:8]
        # print(_IKSN_List)
        _IKSN_List[7] &= 0xE0
        _IKSN_List += [0x00, 0x00]
        _IKSN = bytes(_IKSN_List)
        # print(_IKSN.hex().upper())
        return _IKSN

    ################################################################################################
    # Calculates the IPEK given the inputs
    # _BDK and _KSN are byte type
    ################################################################################################
    def Calculate_IPEK(self, _BDK, _KSN):
        # Get the Initial KSN, the last 21 bits of the transaction counter is set to 0
        _IKSN = self.Get_IKSN(_KSN)

        # Strip the KSN in to Left/Right 8 bytes
        _IKSN_Left_8_Bytes = _IKSN[:DES3.block_size]
        #print("Left = ", end="")
        #print(_IKSN_Left_8_Bytes.hex().upper())

        # 3DES encrypt the Left KSN with the BDK without any mask
        _DES3 = DES3.new(key=_BDK, mode=DES3.MODE_ECB)
        _IKSN_Left_8_Bytes_Cipher = _DES3.encrypt(_IKSN_Left_8_Bytes)
        # print("IPEK Left 8 bytes = ", end="")
        #print(_IKSN_Left_8_Bytes_Cipher.hex().upper())

        # Mask the BDK with the constant below
        _BDK_Mask = b'\xC0\xC0\xC0\xC0\x00\x00\x00\x00\xC0\xC0\xC0\xC0\x00\x00\x00\x00'
        # zip() creates tuples of two iterable variables.
        # Note that the tuples are constructed up to the iterable variable
        # with the lowest number of elements
        _Masked_BDK = bytes([_Byte1 ^ _Byte2 for _Byte1, _Byte2 in zip(_BDK_Mask, _BDK)])
        #print("Masked BDK = ", end="")
        #print(_Masked_BDK.hex().upper())

        # 3DES encrypt the left KSN with the masked BDK
        _DES3 = DES3.new(key=_Masked_BDK, mode=DES3.MODE_ECB)
        _IKSN_Right_8_Bytes_Cipher = _DES3.encrypt(_IKSN_Left_8_Bytes)
        # print("IPEK Right 8 bytes = ", end="")
        #print(_IKSN_Right_8_Bytes_Cipher.hex().upper())
        _IPEK = _IKSN_Left_8_Bytes_Cipher + _IKSN_Right_8_Bytes_Cipher
        return _IPEK

    ################################################################################################
    # Generates the IPEK key givens the BDK and KSN. If BDK and KSN are not given then it gets
    # then from the user
    # _BDK and _KSN are Str type
    ################################################################################################
    def Generate(self, _BDK_Str=None, _KSN_Str=None):
        while True:
            # Get BDK is not already provided
            if _BDK_Str is None:
                _BDK_Str = self.Get_BDK()
            else:
                if len(_BDK_Str) != 32:
                    click.secho("Error : BDK is not 32 hex characters!", fg="red")
                    break
                if not all(_chars in string.hexdigits for _chars in _BDK_Str):
                    click.secho("Error : Non-Hex character found!", fg="red")
                    break

            # Get KSN if not already provided
            if _KSN_Str is None:
                _KSN_Str = self.Get_KSN()
            else:
                if len(_KSN_Str) != 20:
                    click.secho("Error : KSN is not 20 hex characters!", fg="red")
                    break
                if not all(_chars in string.hexdigits for _chars in _KSN_Str):
                    click.secho("Error : Non-Hex character found!", fg="red")
                    break

            _BDK = bytes.fromhex(_BDK_Str)
            _KSN = bytes.fromhex(_KSN_Str)
            _IPEK = self.Calculate_IPEK(_BDK, _KSN)
            break
        return _IPEK

####################################################################################################
# DUKPT class
####################################################################################################
class DUKPT:
    ################################################################################################
    # Constructor
    ################################################################################################
    def __init__(self):
        # Creates a bit array of 0 bits - An empty bit array
        self.IPEK = BitArray(0)
        self.KSN = BitArray(0)
        self.Current_Key = BitArray(0)
        return;

    ################################################################################################
    # Set the KSN
    # _ksn shall be bytes
    ################################################################################################
    def Set_KSN(self, _ksn):
        if len(_ksn) < 10:
            _retval = False
        else:
            self.KSN = BitArray(bytes=_ksn)
            _retval = True
        return _retval

    ################################################################################################
    # Set the IPEK
    # _ipek shall be bytes
    ################################################################################################
    def Set_IPEK(self, _ipek):
        if len(_ipek) < 16:
            _retval = False
        else:
            self.IPEK = BitArray(bytes=_ipek)
            _retval = True
        return _retval

    ################################################################################################
    # Set both IPEK and KSN
    # _ipek and _ksn shall be bytes
    ################################################################################################
    def Set_IPEK_and_KSN(self, _ipek, _ksn):
        if self.Set_IPEK(_ipek) and self.Set_KSN(_ksn):
            _retval = True
        else:
            _retval = False
        return _retval

    ################################################################################################
    # Derives the data encryption key
    ################################################################################################
    def Derive_Key(self):
        """Derive a unique key given the _ipek and _ksn
        Keyword arguments:
        _ipek (BitArray) -- Initial Pin Encryption Key
        _ksn (BitArray)  -- Key Serial Number
        """
        _c_mask = BitArray(hex='0xc0c0c0c000000000c0c0c0c000000000')
        _ksn_offset = 2
        _ctr_offset = -3
        _right_offset = 8

        # Registers taken from documentation
        _curkey = self.IPEK
        _ksnr = BitArray(bytes=self.KSN.bytes[_ksn_offset:])
        # print(_ksnr.hex)

        _r3 = self.Get_TxN_Counter(_ksnr)
        _r8 = self.Reset_TxN_Counter(_ksnr.bytes)
        _sr = BitArray(hex='0x100000')
        # while ((_sr.bytes[0] != '\x00') or (_sr.bytes[1] != '\x00') or (_sr.bytes[2] != '\x00')):
        while _sr.bytes[0] != 0 or _sr.bytes[1] != 0 or _sr.bytes[2] != 0:
            _tmp = self.Get_TxN_Counter(_sr)
            _tmp = _tmp & _r3
            # if (_tmp.bytes[0] != '\x00') or (_tmp.bytes[1] != '\x00') or (_tmp.bytes[2] != '\x00'):
            if (_tmp.bytes[0] != 0) or (_tmp.bytes[1] != 0) or (_tmp.bytes[2] != 0):
                # Step 2
                _n_ctr = BitArray(bytes=_r8.bytes[_ctr_offset:]) | _sr
                _r8 = BitArray(bytes=_r8.bytes[:_ctr_offset] + _n_ctr.bytes)

                # Step 3
                _r8a = _r8 ^ BitArray(bytes=_curkey.bytes[_right_offset:])

                # Step 4
                # ??? Start
                # _cipher = DES.new(_curkey.bytes[:DES.key_size], DES.MODE_ECB)
                _iv = b'\x00\x00\x00\x00\x00\x00\x00\x00'
                _cipher = DES.new(_curkey.bytes[:DES.key_size], DES.MODE_CBC, iv=_iv)
                # ??? End
                _r8a = BitArray(bytes=_cipher.encrypt(_r8a.bytes))

                # Step 5
                _r8a = BitArray(bytes=_curkey.bytes[_right_offset:]) ^ _r8a

                # Step 6
                _curkey = _curkey ^ _c_mask

                # Step 7
                _r8b = BitArray(bytes=_curkey.bytes[_right_offset:]) ^ _r8

                # Step 8
                # ??? Start
                _cipher = DES.new(_curkey.bytes[:DES.key_size], DES.MODE_ECB)
                # _iv = b'\x00\x00\x00\x00\x00\x00\x00\x00'
                # _cipher = DES.new(_curkey.bytes[:DES.key_size], DES.MODE_CBC,  iv =_iv)
                # ??? End
                _r8b = BitArray(bytes=_cipher.encrypt(_r8b.bytes))

                # Step 9
                _r8b = BitArray(bytes=_curkey.bytes[_right_offset:]) ^ _r8b

                # Step 10 / 11
                _curkey = BitArray(bytes=_r8b.bytes + _r8a.bytes)

            _sr >>= 1

        self.Current_Key = _curkey
        _mask = BitArray(bytes=b'\x00\x00\x00\x00\x00\xFF\x00\x00\x00\x00\x00\x00\x00\xFF\x00\x00')
        _variant_key = BitArray(bytes=_curkey.bytes)
        _variant_key ^= _mask
        _DES3 = DES3.new(key=_variant_key.bytes, mode=DES3.MODE_ECB)
        _encryption_key = _DES3.encrypt(_variant_key.bytes)

        return BitArray(bytes=_encryption_key)

    ################################################################################################
    # Resets the TxN counter
    ################################################################################################
    def Reset_TxN_Counter(self, _data):
        """Reset the counter to zero
        Keyword arguments:
        _data (raw or BitArray) -- Must be at least 3 bytes

        Return:
        BitArray of the _data passed in
        """
        if isinstance(_data, BitArray):
            _data = _data.bytes
        if len(_data) < 3:
            return None
        _mask = BitArray(hex='0xe00000')
        _ctr = BitArray(bytes=_data[len(_data) - 3:])
        return BitArray(bytes=_data[:-3] + (_mask & _ctr).bytes)

    ################################################################################################
    # Returns only the counter value
    ################################################################################################
    def Get_TxN_Counter(self, _data):
        """Returns only the counter bytes from a given string or BitArray
        Keyword arguments:
        _data (raw or BitArray) -- Must be at least 3 bytes
        Return:
        BitArray of only the counter bytes
        """
        # print(_data.hex)
        _mask = BitArray(hex='0x1fffff')
        if len(_data.bytes) > 3:
            _ctr = BitArray(bytes=_data.bytes[-3:])
        else:
            _ctr = _data
        return _mask & _ctr

    ################################################################################################
    # Returns only the counter value
    ################################################################################################
    def Increment_TxN_Counter(self):
        """Increase the counter bytes of the stored ksn by one"""
        # ??? Start
        ctr = next(self.KSN.cut(21, start=59)).int + 1
        # ctr = self.KSN.cut(21, start=59).next().int + 1
        # ??? End
        self.KSN.overwrite('0b' + BitArray(int=ctr, length=21).bin, 59)


@click.group()
####################################################################################################
# dukman cli command group
####################################################################################################
def dukman():
    pass

@dukman.command(name="tr31")
####################################################################################################
# Generates IPEK from BDK and KSN input
####################################################################################################
def Generate_TR31():
    print("#" * 80)
    print("# Generating TR31...")
    _tr31 = TR31_Generator()
    _tr31.Generate()
    return

@dukman.command(name="ipek")
####################################################################################################
# Generates IPEK from BDK and KSN input
####################################################################################################
def Generate_IPEK():
    print("#" * 80)
    print("# Generating IPEK...")
    _ipek = IPEK_Generator()
    _IPEK = _ipek.Generate()
    click.secho("IPEK :" + _IPEK.hex().upper(), fg="yellow", bold=True)
    return

@dukman.command(name="hmac")
####################################################################################################
# Generates HMAC
####################################################################################################
def Generate_HMAC():
    print("#" * 80)
    print("# Generating HMAC...")
    _progress_ok = False

    while True:
        _key_str = input("HMAC key : ")
        
        if len(_key_str) != 32:
            click.secho("Error : HMAC key is not 32 hex characters!", fg="red")
            continue
                    
        if not all(_chars in string.hexdigits for _chars in _key_str):
            click.secho("Error : Non-Hex character found!", fg="red")
            continue
        else:
            _progress_ok = True
            break

    while _progress_ok:
        _data_str = input("Data : ")
        
        if len(_data_str) % 2:
            click.secho("Error : Data bytes should have even number of hex characters!", fg="red")
            continue
            
        if not all(_chars in string.hexdigits for _chars in _data_str):
            click.secho("Error : Non-Hex character found!", fg="red")
            continue
        else:
            break

    _key = bytes.fromhex(_key_str)
    _data = bytes.fromhex(_data_str)

    _hmac = HMAC.new(key=_key, msg=_data, digestmod=SHA256)
    click.secho("HMAC : " + _hmac.hexdigest(), fg="yellow", bold=True)
    return


@dukman.command(name="dukpt")
####################################################################################################
# Derives DUKPT keys
####################################################################################################
def Derive_DUKPT_Keys():
    print("#" * 80)
    print("# Deriving DUKPT keys...")
    _progress_ok = False

    while True:
        _ipek_str = input("IPEK : ")

        if len(_ipek_str) != 32:
            click.secho("Error : IPEK is not 32 hex characters!", fg="red")
            continue

        if not all(_chars in string.hexdigits for _chars in _ipek_str):
            click.secho("Error : Non-Hex character found!", fg="red")
            continue
        else:
            _progress_ok = True
            break

    while _progress_ok:
        _ksn_str = input("KSN : ")

        if len(_ksn_str) != 20:
            click.secho("Error : KSN is not 20 hex characters!", fg="red")
            continue

        if not all(_chars in string.hexdigits for _chars in _ksn_str):
            click.secho("Error : Non-Hex character found!", fg="red")
            continue
        else:
            break

    while True:
        _number_of_keys_to_be_derived_str = input("Number of keys to be derived : ")
        if not _number_of_keys_to_be_derived_str.isdigit():
            click.secho("Error : Positive integer value expected!", fg="red")
            continue
        else:
            break

    _ipek = bytes.fromhex(_ipek_str)
    _ksn = bytes.fromhex(_ksn_str)
    _number_of_keys_to_be_derived = int(_number_of_keys_to_be_derived_str)

    _dukpt = DUKPT()
    _dukpt.Set_IPEK_and_KSN(_ipek, _ksn)
    click.secho("IPEK #{}".format(_dukpt.IPEK.hex), fg="green", bold=True)
    for index in range(0, _number_of_keys_to_be_derived):
        _key = _dukpt.Derive_Key()
        click.secho("KSN #{}, Data_Encryption_Key = {}".format(_dukpt.KSN.hex, _key.hex), fg="yellow", bold=True)
        _dukpt.Increment_TxN_Counter()
    return

@dukman.command(name="version")
####################################################################################################
# Prints version information during startup
####################################################################################################
def Print_Version():
    print("#" * 80)
    print("# DUKman v{0}.{1}".format(DUKMAN_MAJOR_VERSION, DUKMAN_MINOR_VERSION))
    print("# This script generates TR31 key blocks, IPEK, HMAC")
    return

if __name__ == "__main__":
    dukman()
